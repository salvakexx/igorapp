@foreach($seriesOptions as $option)
    <option value="{{$option->id}}" @if($selectedSeriesId ?? false && $selectedSeriesId === $option->id) selected @endif>{{$option->name}}</option>
@endforeach
