@php

    /* @var $manufacturerBlocks \App\SectionViewBuilder\ManufacturerBlock */
    $manufacturerBlocks = $sectionViewBuilder->manufacturerBlocks;
@endphp

@extends('catalog.layout')

@section('content')

    @include('catalog.partials.catalog_link')
    <div class="content-my">

        @if(!empty($manufacturerBlocks))

    @foreach($manufacturerBlocks as $manufacturerBlock)
    <div class="row">
        <div class="col-lg-12 one-manufacturer-block">
            <section class="panel">
                @if($manufacturerBlock->manufacturer->logo)
                    <div class="text-center">
                        <img class="img-thumbnail" style="border: none" src="/storage/{{$manufacturerBlock->manufacturer->logo}}">
                    </div>
                @endif

                @foreach($manufacturerBlock->series as $serieBlock)
                <section class="panel">
                    <header class="panel-heading">
                        {!!  $serieBlock->serie->name!!}
                    </header>
                    @if($serieBlock->serie->image)
                        <div class="text-center">
                            <img  style="border: none" class="img-thumbnail" src="/storage/{{$serieBlock->serie->image}}">
                        </div>
                    @endif
                    <div class="">
                        <table class="table">
                            <thead>
                            <tr>
                                <th >Наименование</th>
                                <th style="text-align: center">Розница</th>
                                @if($section->id != 3)
                                <th style="text-align: center">Опт</th>
                                @endif
                                @if($section->id == 3)
                                <th style="text-align: center">Грн.</th>
                                @endif
                                <th style="text-align: center">Наличие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($serieBlock->products as $key => $product)
                            <tr>
                                <td style="word-wrap: break-word; max-width: 25%">{{$product->name}}</td>
                                <td style="text-align: center">{{(int)$product->wholesale_price}}$</td>
                                @if($section->id != 3)
                                <td style=" text-align:center;color: red">{{(int)$product->retail_price}}$</td>
                                @endif
                                @if($section->id == 3)
                                <td style=" text-align:center;color: red">{{(int)($product->retail_price * (float)setting('site.course'))}}</td>
                                @endif
                                <td style="text-align: center">{!! $product->available ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times-circle"></i>' !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
                @endforeach

            </section>
        </div>
    </div>
    @endforeach
            @else
            <div class="text-center">Раздел находится в разработке</div>
        @endif
    </div>
@endsection
