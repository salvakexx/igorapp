<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title></title>

    <!-- Bootstrap CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-img3-body">

<div class="container">

    <form class="login-form" method="POST" action="{{url('/login')}}">
        {{ csrf_field() }}
        <div class="login-wrap">
            <p class="login-img">
            <img class="img-thumbnail" style="background-color: transparent; " src="/img/logo.png">
            </p>

            @if(!$errors->isEmpty())
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $err)
                            <li>{{ $err }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="input-group @if($errors->has('email')) has-error @endif">
                <span class="input-group-addon"><i class="icon_phone"></i></span>
                <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Телефон" autofocus>
            </div>
            <div class="input-group @if($errors->has('password')) has-error @endif">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" name="password" class="form-control" placeholder="Пароль">
            </div>
            <label class="checkbox">

                {{--<input type="checkbox" name="remember" value="1"> Запомнить?--}}
                {{--<span class="pull-right"> <a href="#"> Забыли пароль?</a></span>--}}
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Вход</button>
        </div>
    </form>

</div>


<!-- javascripts -->
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.mask.min.js"></script>
<script>
    $(document).ready(function(){
        $.jMaskGlobals = {translation: {
            'n': {pattern: /\d/},
            // '0': {pattern: /0/},
        }
        };
        $('[name="email"]').mask("+38(0nn)nnn-nn-nn");
    });
</script>

</body>

</html>
