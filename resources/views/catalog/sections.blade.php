@extends('catalog.layout')


@section('content')
    <div class="row">
        @foreach($sections as $key => $section)
        <div class="col-lg-4 col-sm-4 col-xs-4 section-block-main">
            <a href="/section/{{$section->id}}">
            <section class="panel">
                <header class="panel-heading section-title-main" style="border: none">
                    {{$section->name}}
                </header>
                @if($section->image)
                    <div class="text-center">
                        <img class="img-thumbnail" style="border: none" src="/storage/{{$section->image}}">
                    </div>
                @endif
            </section>
            </a>
        </div>
            @if(($key+1) % 3 === 0)
                <div style="clear: both"></div>
            @endif
        @endforeach
    </div>
@endsection
