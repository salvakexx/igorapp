<!DOCTYPE html>
<html lang="en" style="overflow: auto">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>КЛІМАТТЕХ</title>

    <!-- Bootstrap CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="/css/bootstrap-theme.css" rel="stylesheet">
    <!-- external css -->
    <!-- font icon -->
    <link href="/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/style-responsive.css" rel="stylesheet" />
</head>

<body >
<!-- container section start -->
<section id="container" class="">
    <!--header start-->
    <header class="header" style="display: block">
        <!--logo start-->
        <div class="top-nav notification-row">
            <!-- notificatoin dropdown start-->
            <ul class="nav pull-right top-menu">
               <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown"  style="color: black" class="dropdown-toggle" href="#">
                        <span class="username" style="color: black">{{ auth()->user()->name }}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href="/logout"><i class="icon_key_alt"></i>Выйти</a>
                        </li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
            <!-- notificatoin dropdown end-->
        </div>
    </header>

    <div class="text-center" style="position: relative">
        <a href="/"  style="">
            <img class="img-thumbnail" style="border:none; background-color: transparent; " src="/img/logo.png">
        </a>

    </div>

    <div style="clear: both"></div>
    <!--header end-->
    <!--logo end-->
    <section class="wrapper my-wrapper">
        {{--<div class="row">--}}
            {{--<div class="col-lg-12">--}}
                {{--<h3 class="page-header"><i class="icon_genius"></i> Widgets</h3>--}}
                {{--<ol class="breadcrumb">--}}
                    {{--<li><i class="fa fa-home"></i><a href="index.html">Home</a></li>--}}
                    {{--<li><i class="icon_genius"></i>Widgets</li>--}}
            {{--</div>--}}
        {{--</div>--}}
        @yield('content')
    </section>
</section>

<!-- javascripts -->
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="/js/jquery.scrollTo.min.js"></script>
<script src="/js/jquery.nicescroll.js" type="text/javascript"></script>
<!-- jquery knob -->
<script src="/js/jquery.knob.js"></script>
<!--custome script for all page-->
<script src="/js/scripts.js"></script>

<script>
    //knob
    $(".knob").knob();
</script>

<style>
    /* Style the header */
    .header-sticky {
        padding: 10px 16px;
        z-index: 9999;
        /*background: #555;*/
        /*color: #f1f1f1;*/
    }

    /* Page content */
    .content-my {
        /*padding: 16px;*/

        min-width: 320px;
    }

    /* The sticky class is added to the header with JS when it reaches its scroll position */
    .sticky {
        position: fixed;
        top: 0;
        width: 100%
    }

    /* Add some top padding to the page content to prevent sudden quick movement (as the header gets a new position at the top of the page (position:fixed and top:0) */
    .sticky + .content-my {
        padding-top: 102px;
    }
</style>
<script>
    // Get the header
    var header = document.getElementById("myHeader");


    if(header) {

        // When the user scrolls the page, execute myFunction
        window.onscroll = function() {myFunction()};
        // Get the offset position of the navbar
        var sticky = header.offsetTop;
    }
    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
</script>

</body>

</html>
