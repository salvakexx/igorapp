

@extends('catalog.layout')

@section('content')
    @include('catalog.partials.news_link')
    <div class="content-my">
        <div class="row">
            <h3>{{$news->title}}</h3>
        </div>
        <div class="row col-12 page-content">
            {{--<img src="/storage/{{$news->image}}" style="float: right; margin: 10px;">--}}
            {!! $news->content !!}
        </div>
    </div>
@endsection
