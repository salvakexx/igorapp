

@extends('catalog.layout')

@section('content')
    @include('catalog.partials.catalog_link')
    <div class="content-my">
        <div class="row">
            @foreach($news as $oneNews)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle">
                                {{$oneNews->title}} - <small><b>{{date('d.m.Y', strtotime($oneNews->created_at))}}</b></small>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <img style="width:100px; float: left; margin: 10px" src="/storage/{{$oneNews->image}}">
                            {{$oneNews->short_content}}
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <a style="" href="/news/{{$oneNews->id}}">Подробнее>></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
