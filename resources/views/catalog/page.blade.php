

@extends('catalog.layout')

@section('content')
    @include('catalog.partials.catalog_link')
    <div class="content-my">
        <div class="row page-content">
            {!! $section->page_content !!}
        </div>
    </div>
@endsection
