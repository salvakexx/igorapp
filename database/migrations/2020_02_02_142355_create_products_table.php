<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('measure')->nullable();
            $table->string('image')->nullable();
            $table->boolean('available')->nullable();
            $table->string('available_city')->nullable();
            $table->decimal('wholesale_price', 9, 2)->nullable();
            $table->decimal('retail_price', 9, 2)->nullable();
            $table->decimal('uah_price', 9, 2)->nullable();
            $table->bigInteger('manufacturer_id')->nullable();
            $table->bigInteger('series_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
