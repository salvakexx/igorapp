<?php
/**
 * Created by PhpStorm.
 * User: kexx
 * Date: 2/3/20
 * Time: 12:18 AM
 */

namespace App\SectionViewBuilder;


use App\Product;
use App\Section;

class SectionViewBuilder
{
    /**
     * @var ManufacturerBlock[]
     */
    public $manufacturerBlocks;

    public function __construct($sectionWithProducts)
    {
        $this->initBySectionWithProducts($sectionWithProducts);
    }

    /**
     * @param Section $sectionWithProducts
     */
    public function initBySectionWithProducts($sectionWithProducts)
    {
        /* @var $products Product[] */
        $products = $sectionWithProducts->products;
        $manufacturerIds = array_unique($products->pluck('manufacturer_id')->toArray());
        foreach ($manufacturerIds as $manufacturerId) {
            $this->manufacturerBlocks[] = new ManufacturerBlock($products->filter(function($item) use ($manufacturerId) {
                return $item->manufacturer_id === $manufacturerId;
            }));
        }
        $this->manufacturerBlocks = collect($this->manufacturerBlocks)->sortByDesc(function (ManufacturerBlock $block) {
            return $block->manufacturer->order;
        });
    }

}
