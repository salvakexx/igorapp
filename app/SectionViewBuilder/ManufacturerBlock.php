<?php
/**
 * Created by PhpStorm.
 * User: kexx
 * Date: 2/3/20
 * Time: 12:18 AM
 */

namespace App\SectionViewBuilder;


use App\Manufacturer;
use App\Product;

class ManufacturerBlock
{

    /**
     * @var SeriesBlock[]
     */
    public $series;

    /**
     * @var Manufacturer
     */
    public $manufacturer;

    public function __construct($products)
    {
        $this->initByProducts($products);
    }

    /**
     * @param Product[] $products
     */
    public function initByProducts($products)
    {
        $this->manufacturer = $products->first() ? $products->first()->manufacturer : new Manufacturer();
        $seriesIds = array_unique($products->pluck('series_id')->toArray());

        foreach ($seriesIds as $seriesId) {
            $this->series[] = new SeriesBlock($products->filter(function($item) use ($seriesId) {
                return $item->series_id === $seriesId;
            }));
        }
    }
}
