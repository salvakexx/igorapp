<?php

namespace App\SectionViewBuilder;

use App\Product;
use App\Serie;

class SeriesBlock
{
    /**
     * @var Product[]
     */
    public $products;

    /**
     * @var Serie
     */
    public $serie;

    public function __construct($products)
    {
        $this->products = $products;
        $this->serie = $products->first() ? ($products->first()->serie ?: new Serie()) : new Serie();
    }
}
