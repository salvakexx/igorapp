<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function sections()
    {
        return $this->belongsToMany(Section::class);
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function serie()
    {
        return $this->belongsTo(Serie::class, 'series_id');
    }
}
