<?php

namespace App;

use TCG\Voyager\Models\Role;

class Client extends User
{
    protected $table = 'users';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('hasClientRole', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->whereHas('roles', function ($query) {
                $query->where('name', '=', 'client');
            });
        });

        static::created(function($model)
        {
            $model->roles()->attach(Role::whereName('client')->first()->id);
        });
    }
}
