<?php
/**
 * Created by PhpStorm.
 * User: kexx
 * Date: 2/5/20
 * Time: 9:43 PM
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function rules()
    {
        $id = $this->route('user');
        return [
            'email' => 'required|unique:users'.($id !== null ? (','.$id) : ''),
            'phone' => 'required|unique:users'.($id !== null ? (','.$id) : ''),
        ];
    }
}
