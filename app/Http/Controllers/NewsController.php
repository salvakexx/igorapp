<?php

namespace App\Http\Controllers;

use App\News;
use App\Section;
use App\SectionViewBuilder\SectionViewBuilder;

class NewsController extends Controller
{
    public function show($newsId)
    {
        $news = News::findOrFail($newsId);
        return view('catalog.one_blog', compact('news'));
    }
}
