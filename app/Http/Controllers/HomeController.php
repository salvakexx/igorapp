<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Section;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class HomeController extends Controller
{
   public function index()
   {
       $sections = Section::orderBy('order','DESC')->get();

       return view('catalog.sections', compact('sections'));
   }
}
