<?php

namespace App\Http\Controllers;

use App\News;
use App\Section;
use App\SectionViewBuilder\SectionViewBuilder;

class SectionController extends Controller
{
    public function show($sectionId)
    {
        $section = Section::findOrFail($sectionId)->load(['products.manufacturer', 'products.serie']);
        if($section->is_page) {
            return view('catalog.page', compact('section'));
        }
        if($section->is_blog) {
            return $this->showBlog();
        }
        $sectionViewBuilder = new SectionViewBuilder($section);
        return view('catalog.section', compact('section', 'sectionViewBuilder'));
    }

    public function showBlog() {
        $news = News::orderBy('created_at','DESC')->get();
        return view('catalog.blog', compact('news'));
    }
}
