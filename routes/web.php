<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'],function(){
    Route::get('/', 'HomeController@index');

    Route::get('/section/{sectionId}', 'SectionController@show');

    Route::get('/news/{newsId}', 'NewsController@show');
    Route::get('/home', function (){return redirect()->to('/');});

});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/products/getSeriesOptions/{manufacturerId}', 'ProductsController@getSeriesByManufacturerOptions');
});
Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();
